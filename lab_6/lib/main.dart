import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(backgroundColor: Colors.teal.shade50),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.teal.shade400,
            title: const Text('Lab_6'),
          ),
          body: Container(
              child: const MyStatelessWidget(), alignment: Alignment.topCenter),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              // Add your onPressed code here!
            },
            child: const Icon(Icons.add),
            backgroundColor: Colors.teal.shade400,
          ),
          drawer: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.teal,
                  ),
                  child: Text('P.B.P'),
                ),
                ListTile(
                  title: const Text('Susun Jadwal'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Quiz of Pandemic'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Scheduler'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Recently on Education'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ));
  }
}

class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Nama',
          ),
        ),
        DataColumn(
          label: Text(
            'Tanggal',
          ),
        ),
        DataColumn(
          label: Text(
            'Description',
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Kumpul Tugas PBP')),
            DataCell(Text('18, November')),
            DataCell(Text('kumpulin di git'), showEditIcon: true),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('MPPI E Dedline')),
            DataCell(Text('26, November')),
            DataCell(Text('kerjain woi'), showEditIcon: true),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Quiz OS')),
            DataCell(Text('22, November')),
            DataCell(Text('Belajar woi'), showEditIcon: true),
          ],
        ),
      ],
    );
  }
}
