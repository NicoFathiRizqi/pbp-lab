from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm


def index(request):
    note = Note.objects.all().values()
    response = {'Note' : note}
    return render(request, 'lab4_index.html', response)

def add_form(request):
    context = {}

    form = NoteForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        # save the form data to model
        form.save()
        form = NoteForm()

    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'Note' : note}
    return render(request, 'lab4_note_list.html', response)
