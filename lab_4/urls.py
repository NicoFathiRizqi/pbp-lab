from django.urls import path
from .views import index, add_form, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add', add_form, name='add'),
    path('note-list', note_list, name='note-list')
]
