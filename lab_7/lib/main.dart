import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(backgroundColor: Colors.teal.shade50),
      title: "Add Event",
      home: BelajarForm(),
    );
  }
}

class BelajarForm extends StatefulWidget {
  const BelajarForm({Key? key}) : super(key: key);
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal.shade400,
        title: Text("Add Event"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // tambahkan komponen seperti input field disini
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextFormField(
                  decoration: new InputDecoration(
                    hintText: "Tambah event",
                    labelText: "Event",
                    icon: Icon(Icons.people),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value != null && value.isEmpty) {
                      return 'Nama tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextFormField(
                  decoration: new InputDecoration(
                    hintText: "Tambahkan waktu event",
                    labelText: "Time",
                    icon: Icon(Icons.people),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value != null && value.isEmpty) {
                      return 'Waktu tidak boleh kosong';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  decoration: new InputDecoration(
                    hintText: "Tulis detail event",
                    labelText: "Description",
                    icon: Icon(Icons.people),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                ),
              ),
              ElevatedButton(
                  child: Text(
                    "Add Event",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      print("Event successfully added");
                    } else {
                      print("Invalid Input. Please try again");
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
