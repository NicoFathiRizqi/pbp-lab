from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    note = Note.objects.all().values()
    response = {'Note' : note}
    return render(request, 'lab2.html', response)

def xml(request):
    note = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")


def json(request):
    note = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
# Create your views here.