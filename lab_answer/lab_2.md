**1. Apakah perbedaan antara JSON dan XML?**  
***

Baik JSON dan XML keduanya adalah format yang digunakan untuk data delivery. Namun ada sedikit perbedaan dalam penerapannya.

XML (eXtended Markup Language) merupakan bagian dari SGML sehingga *code* nya dibuat menggunakan bahasa markup. Dalam implementasinya XML akan membuat suatu *document tree*, yang nantinya data-data kan didefinisikan dengan *tag* sendiri menggunakan "<>". *Document tree* dimulai dari *root element* dan diakhiri dengan *child element*. 

Sementara JSON merupakan singkatan dari JavaScript Oriented Notation. Dari namanya saja kita bisa tahu bahwa JSON berbasis bahasa JavaScript. JSON memetakan data yang ada dengan key-map value dan memiliki sintaks semantik yang lebih bagus dari XML. Namun JSON memiliki kekurangan salah satunya adalah keamanannya lebih rendah dibangingkan XML.

**2. Apakah perbedaan antara HTML dan XML?**
***
HTML dan XML saling bekerja sama dalam pembuatan halaman web. Namun kedua fungsinya berbeda

HTML merupakan singkatan dari HyperText Markup Language yang fungsinya adalah menampilkan halaman web dari data yang ada. HTML fokus kepada menampilkan data dan memiliki kemampuan untuk melakukan *formatting* terhadap tampilannya. Dalam implementasinya HTML menggunakan *tags* dalam "<>" yang sudah ada.

Di sisi lain XML dibuat untuk membawa data dengan fokus kepada data itu sendiri, bukan tampilannya. Dalam implementasinya XML menggunakan *tags* dalam "<>" yang belum ditentukan sebelumnya. Isi tags ini tergantung dari keputusan *programmer* yang mengurus website.